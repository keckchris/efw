package org.srs.apps.web.rest;

import nl.jqno.equalsverifier.Warning;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;
import org.srs.apps.EfwApp;
import org.srs.apps.domain.Employee;
import org.srs.apps.domain.FeelWheel;
import org.srs.apps.domain.Feeling;
import org.srs.apps.repository.FeelWheelRepository;
import org.srs.apps.repository.search.FeelWheelSearchRepository;
import org.srs.apps.service.FeelWheelService;
import org.srs.apps.service.dto.FeelWheelDTO;
import org.srs.apps.service.mapper.FeelWheelMapper;
import org.srs.apps.web.rest.errors.ExceptionTranslator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.srs.apps.web.rest.TestUtil.createFormattingConversionService;

/**
 * Test class for the FeelWheelResource REST controller.
 *
 * @see FeelWheelResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = EfwApp.class)
public class FeelWheelResourceIntTest {

    private static final String DEFAULT_SUBJECT = "AAAAAAAAAA";
    private static final String UPDATED_SUBJECT = "BBBBBBBBBB";

    private static final Instant DEFAULT_FROM = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FROM = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_TO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TO = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private FeelWheelRepository feelWheelRepository;

    @Autowired
    private FeelWheelMapper feelWheelMapper;

    @Autowired
    private FeelWheelService feelWheelService;

    /**
     * This repository is mocked in the org.srs.apps.repository.search test package.
     *
     * @see org.srs.apps.repository.search.FeelWheelSearchRepositoryMockConfiguration
     */
    @Autowired
    private FeelWheelSearchRepository mockFeelWheelSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restFeelWheelMockMvc;

    private FeelWheel feelWheel;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FeelWheelResource feelWheelResource = new FeelWheelResource(feelWheelService);
        this.restFeelWheelMockMvc = MockMvcBuilders.standaloneSetup(feelWheelResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FeelWheel createEntity(EntityManager em) {
        return new FeelWheel().toBuilder()
            .subject(DEFAULT_SUBJECT)
            .from(DEFAULT_FROM)
            .to(DEFAULT_TO).build();
    }

    @Before
    public void initTest() {
        feelWheel = createEntity(em);
    }

    @Test
    @Transactional
    public void createFeelWheel() throws Exception {
        int databaseSizeBeforeCreate = feelWheelRepository.findAll().size();

        // Create the FeelWheel
        FeelWheelDTO feelWheelDTO = feelWheelMapper.toDto(feelWheel);
        restFeelWheelMockMvc.perform(post("/api/feel-wheels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(feelWheelDTO)))
            .andExpect(status().isCreated());

        // Validate the FeelWheel in the database
        List<FeelWheel> feelWheelList = feelWheelRepository.findAll();
        assertThat(feelWheelList).hasSize(databaseSizeBeforeCreate + 1);
        FeelWheel testFeelWheel = feelWheelList.get(feelWheelList.size() - 1);
        assertThat(testFeelWheel.getSubject()).isEqualTo(DEFAULT_SUBJECT);
        assertThat(testFeelWheel.getFrom()).isEqualTo(DEFAULT_FROM);
        assertThat(testFeelWheel.getTo()).isEqualTo(DEFAULT_TO);

        // Validate the FeelWheel in Elasticsearch
        verify(mockFeelWheelSearchRepository, times(1)).save(testFeelWheel);
    }

    @Test
    @Transactional
    public void createFeelWheelWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = feelWheelRepository.findAll().size();

        // Create the FeelWheel with an existing ID
        feelWheel.setId(1L);
        FeelWheelDTO feelWheelDTO = feelWheelMapper.toDto(feelWheel);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFeelWheelMockMvc.perform(post("/api/feel-wheels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(feelWheelDTO)))
            .andExpect(status().isBadRequest());

        // Validate the FeelWheel in the database
        List<FeelWheel> feelWheelList = feelWheelRepository.findAll();
        assertThat(feelWheelList).hasSize(databaseSizeBeforeCreate);

        // Validate the FeelWheel in Elasticsearch
        verify(mockFeelWheelSearchRepository, times(0)).save(feelWheel);
    }

    @Test
    @Transactional
    public void checkSubjectIsRequired() throws Exception {
        int databaseSizeBeforeTest = feelWheelRepository.findAll().size();
        // set the field null
        feelWheel.setSubject(null);

        // Create the FeelWheel, which fails.
        FeelWheelDTO feelWheelDTO = feelWheelMapper.toDto(feelWheel);

        restFeelWheelMockMvc.perform(post("/api/feel-wheels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(feelWheelDTO)))
            .andExpect(status().isBadRequest());

        List<FeelWheel> feelWheelList = feelWheelRepository.findAll();
        assertThat(feelWheelList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFeelWheels() throws Exception {
        // Initialize the database
        feelWheelRepository.saveAndFlush(feelWheel);

        // Get all the feelWheelList
        restFeelWheelMockMvc.perform(get("/api/feel-wheels?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(feelWheel.getId().intValue())))
            .andExpect(jsonPath("$.[*].subject").value(hasItem(DEFAULT_SUBJECT.toString())))
            .andExpect(jsonPath("$.[*].from").value(hasItem(DEFAULT_FROM.toString())))
            .andExpect(jsonPath("$.[*].to").value(hasItem(DEFAULT_TO.toString())));
    }
    
    @Test
    @Transactional
    public void getFeelWheel() throws Exception {
        // Initialize the database
        feelWheelRepository.saveAndFlush(feelWheel);

        // Get the feelWheel
        restFeelWheelMockMvc.perform(get("/api/feel-wheels/{id}", feelWheel.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(feelWheel.getId().intValue()))
            .andExpect(jsonPath("$.subject").value(DEFAULT_SUBJECT.toString()))
            .andExpect(jsonPath("$.from").value(DEFAULT_FROM.toString()))
            .andExpect(jsonPath("$.to").value(DEFAULT_TO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingFeelWheel() throws Exception {
        // Get the feelWheel
        restFeelWheelMockMvc.perform(get("/api/feel-wheels/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFeelWheel() throws Exception {
        // Initialize the database
        feelWheelRepository.saveAndFlush(feelWheel);

        int databaseSizeBeforeUpdate = feelWheelRepository.findAll().size();

        // Update the feelWheel
        FeelWheel updatedFeelWheel = feelWheelRepository.findById(feelWheel.getId()).get();
        // Disconnect from session so that the updates on updatedFeelWheel are not directly saved in db
        em.detach(updatedFeelWheel);
        updatedFeelWheel=updatedFeelWheel.toBuilder()
            .subject(UPDATED_SUBJECT)
            .from(UPDATED_FROM)
            .to(UPDATED_TO).build();
        FeelWheelDTO feelWheelDTO = feelWheelMapper.toDto(updatedFeelWheel);

        restFeelWheelMockMvc.perform(put("/api/feel-wheels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(feelWheelDTO)))
            .andExpect(status().isOk());

        // Validate the FeelWheel in the database
        List<FeelWheel> feelWheelList = feelWheelRepository.findAll();
        assertThat(feelWheelList).hasSize(databaseSizeBeforeUpdate);
        FeelWheel testFeelWheel = feelWheelList.get(feelWheelList.size() - 1);
        assertThat(testFeelWheel.getSubject()).isEqualTo(UPDATED_SUBJECT);
        assertThat(testFeelWheel.getFrom()).isEqualTo(UPDATED_FROM);
        assertThat(testFeelWheel.getTo()).isEqualTo(UPDATED_TO);

        // Validate the FeelWheel in Elasticsearch
        verify(mockFeelWheelSearchRepository, times(1)).save(testFeelWheel);
    }

    @Test
    @Transactional
    public void updateNonExistingFeelWheel() throws Exception {
        int databaseSizeBeforeUpdate = feelWheelRepository.findAll().size();

        // Create the FeelWheel
        FeelWheelDTO feelWheelDTO = feelWheelMapper.toDto(feelWheel);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFeelWheelMockMvc.perform(put("/api/feel-wheels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(feelWheelDTO)))
            .andExpect(status().isBadRequest());

        // Validate the FeelWheel in the database
        List<FeelWheel> feelWheelList = feelWheelRepository.findAll();
        assertThat(feelWheelList).hasSize(databaseSizeBeforeUpdate);

        // Validate the FeelWheel in Elasticsearch
        verify(mockFeelWheelSearchRepository, times(0)).save(feelWheel);
    }

    @Test
    @Transactional
    public void deleteFeelWheel() throws Exception {
        // Initialize the database
        feelWheelRepository.saveAndFlush(feelWheel);

        int databaseSizeBeforeDelete = feelWheelRepository.findAll().size();

        // Get the feelWheel
        restFeelWheelMockMvc.perform(delete("/api/feel-wheels/{id}", feelWheel.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<FeelWheel> feelWheelList = feelWheelRepository.findAll();
        assertThat(feelWheelList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the FeelWheel in Elasticsearch
        verify(mockFeelWheelSearchRepository, times(1)).deleteById(feelWheel.getId());
    }

    @Test
    @Transactional
    public void searchFeelWheel() throws Exception {
        // Initialize the database
        feelWheelRepository.saveAndFlush(feelWheel);
        when(mockFeelWheelSearchRepository.search(queryStringQuery("id:" + feelWheel.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(feelWheel), PageRequest.of(0, 1), 1));
        // Search the feelWheel
        restFeelWheelMockMvc.perform(get("/api/_search/feel-wheels?query=id:" + feelWheel.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(feelWheel.getId().intValue())))
            .andExpect(jsonPath("$.[*].subject").value(hasItem(DEFAULT_SUBJECT)))
            .andExpect(jsonPath("$.[*].from").value(hasItem(DEFAULT_FROM.toString())))
            .andExpect(jsonPath("$.[*].to").value(hasItem(DEFAULT_TO.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.PrefabValues<FeelWheel> prefab=TestUtil.createPrefabValues(FeelWheel.class,new FeelWheel().toBuilder().id(1L).build(),new FeelWheel().toBuilder().id(2L).build());
        TestUtil.PrefabValues<Employee> prefabTwo=TestUtil.createPrefabValues(Employee.class,new Employee().toBuilder().id(1L).build(),new Employee().toBuilder().id(2L).build());
        TestUtil.PrefabValues<Feeling> prefabThree=TestUtil.createPrefabValues(Feeling.class,new Feeling().toBuilder().id(1L).build(),new Feeling().toBuilder().id(2L).build());

        TestUtil.equalsVerifier(FeelWheel.class, prefab,prefabTwo,prefabThree, Warning.SURROGATE_KEY);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(FeelWheelDTO.class);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(feelWheelMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(feelWheelMapper.fromId(null)).isNull();
    }
}
