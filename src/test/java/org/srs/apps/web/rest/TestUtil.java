package org.srs.apps.web.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.*;
import lombok.experimental.UtilityClass;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.springframework.format.datetime.standard.DateTimeFormatterRegistrar;
import org.springframework.format.support.DefaultFormattingConversionService;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.http.MediaType;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;

/**
 * Utility class for testing REST controllers.
 */
@UtilityClass
public final class TestUtil {

    private static final ObjectMapper mapper = createObjectMapper();

    /** MediaType for JSON UTF8 */
    static final MediaType APPLICATION_JSON_UTF8 = new MediaType(
            MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), StandardCharsets.UTF_8);


    private static ObjectMapper createObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        mapper.registerModule(new JavaTimeModule());
        return mapper;
    }

    /**
     * Convert an object to JSON byte array.
     *
     * @param object
     *            the object to convert
     * @return the JSON byte array
     * @throws IOException
     */
    public static byte[] convertObjectToJsonBytes(Object object)
            throws IOException {
        return mapper.writeValueAsBytes(object);
    }

    /**
     * Create a byte array with a specific size filled with specified data.
     *
     * @param size the size of the byte array
     * @param data the data to put in the byte array
     * @return the JSON byte array
     */
    public static byte[] createByteArray(int size, String data) {
        byte[] byteArray = new byte[size];
        for (int i = 0; i < size; i++) {
            byteArray[i] = Byte.parseByte(data, 2);
        }
        return byteArray;
    }

    /**
     * A matcher that tests that the examined string represents the same instant as the reference datetime.
     */
    public static class ZonedDateTimeMatcher extends TypeSafeDiagnosingMatcher<String> {

        private final ZonedDateTime date;

        public ZonedDateTimeMatcher(ZonedDateTime date) {
            this.date = date;
        }

        @Override
        protected boolean matchesSafely(String item, Description mismatchDescription) {
            try {
                if (!date.isEqual(ZonedDateTime.parse(item))) {
                    mismatchDescription.appendText("was ").appendValue(item);
                    return false;
                }
                return true;
            } catch (DateTimeParseException e) {
                mismatchDescription.appendText("was ").appendValue(item)
                    .appendText(", which could not be parsed as a ZonedDateTime");
                return false;
            }

        }

        @Override
        public void describeTo(Description description) {
            description.appendText("a String representing the same Instant as ").appendValue(date);
        }
    }

    /**
     * Creates a matcher that matches when the examined string reprensents the same instant as the reference datetime
     * @param date the reference datetime against which the examined string is checked
     */
    public static ZonedDateTimeMatcher sameInstant(ZonedDateTime date) {
        return new ZonedDateTimeMatcher(date);
    }
    static <T,S> void equalsVerifier(Class<T> clazz,Warning warning)  {

        EqualsVerifier.forClass(clazz).usingGetClass().suppress(Warning.NONFINAL_FIELDS,warning).verify();
    }

    /**
     * Verifies the equals/hashcode contract on the domain object.
 * @param clazz
     */
    static <T> void equalsVerifier(Class<T> clazz) {
        equalsVerifier(clazz, null,null ,null );
    }

    /**
     * Verifies the equals/hashcode contract on the domain object.
 * @param clazz
 * @param prefab
 * @param prefabTwo
     */
    static <T,S,V> void equalsVerifier(Class<T> clazz, PrefabValues<S> prefab, PrefabValues<V> prefabTwo) {
        equalsVerifier(clazz, prefab, prefabTwo, null);
    }

    /**
     * Verifies the equals/hashcode contract on the domain object.
 * @param clazz
 * @param prefab
     */
    static <T,S> void equalsVerifier(Class<T> clazz, PrefabValues<S> prefab) {
        equalsVerifier(clazz, prefab, null,null );
    }

    /**
     * Verifies the equals/hashcode contract on the domain object.
     */
    static <T,S,V,R> void equalsVerifier(Class<T> clazz, PrefabValues<S> prefab, PrefabValues<V> prefabTwo, PrefabValues<R> prefabThree)  {
        var verifier=EqualsVerifier.forClass(clazz).usingGetClass();
        if (prefab!=null){
            verifier=verifier.withPrefabValues(prefab.clazz,prefab.valueOne,prefab.valueTwo);
        }
        if (prefabTwo!=null){
            verifier=verifier.withPrefabValues(prefabTwo.clazz,prefabTwo.valueOne,prefabTwo.valueTwo).withPrefabValues(prefabThree.clazz,prefabThree.valueOne,prefabThree.valueTwo);

        }
        if (prefabThree!=null){
            verifier=verifier.withPrefabValues(prefabThree.clazz,prefabThree.valueOne,prefabThree.valueTwo);
        }
        verifier.suppress(Warning.NONFINAL_FIELDS).verify();        
    }

    /**
     * Verifies the equals/hashcode contract on the domain object.
     */
    static <T,S,V,R> void equalsVerifier(Class<T> clazz, PrefabValues<S> prefab, PrefabValues<V> prefabTwo, PrefabValues<R> prefabThree,Warning warning)  {
        var verifier=EqualsVerifier.forClass(clazz).usingGetClass();
        if (prefab!=null){
            verifier=verifier.withPrefabValues(prefab.clazz,prefab.valueOne,prefab.valueTwo);
        }
        if (prefabTwo!=null){
            verifier=verifier.withPrefabValues(prefabTwo.clazz,prefabTwo.valueOne,prefabTwo.valueTwo).withPrefabValues(prefabThree.clazz,prefabThree.valueOne,prefabThree.valueTwo);

        }
        if (prefabThree!=null){
            verifier=verifier.withPrefabValues(prefabThree.clazz,prefabThree.valueOne,prefabThree.valueTwo);
        }
        verifier.suppress(Warning.NONFINAL_FIELDS,warning).verify();
    }
    /**
     * Create a FormattingConversionService which use ISO date format, instead of the localized one.
     * @return the FormattingConversionService
     */
    public static FormattingConversionService createFormattingConversionService() {
        DefaultFormattingConversionService dfcs = new DefaultFormattingConversionService ();
        DateTimeFormatterRegistrar registrar = new DateTimeFormatterRegistrar();
        registrar.setUseIsoFormat(true);
        registrar.registerFormatters(dfcs);
        return dfcs;
    }
    static <T> PrefabValues<T> createPrefabValues(Class<T> clazz, T valueOne, T valueTwo){
        return new PrefabValues<T>().toBuilder().clazz(clazz).valueOne(valueOne).valueTwo(valueTwo).build();
    }
    @Builder(toBuilder = true)
    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    class PrefabValues<T>{
        private Class<T> clazz;
        private T valueOne;
        private T valueTwo;
    }

}
