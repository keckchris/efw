package org.srs.apps.cucumber.stepdefs;

import org.srs.apps.EfwApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = EfwApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
