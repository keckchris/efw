package org.srs.apps.repository.search;

import org.srs.apps.domain.Feeling;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Feeling entity.
 */
public interface FeelingSearchRepository extends ElasticsearchRepository<Feeling, Long> {
}
