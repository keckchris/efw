package org.srs.apps.repository.search;

import org.srs.apps.domain.FeelWheel;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the FeelWheel entity.
 */
public interface FeelWheelSearchRepository extends ElasticsearchRepository<FeelWheel, Long> {
}
