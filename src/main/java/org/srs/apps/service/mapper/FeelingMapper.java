package org.srs.apps.service.mapper;

import org.srs.apps.domain.*;
import org.srs.apps.service.dto.FeelingDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Feeling and its DTO FeelingDTO.
 */
@Mapper(componentModel = "spring", uses = {FeelWheelMapper.class})
public interface FeelingMapper extends EntityMapper<FeelingDTO, Feeling> {

    @Mapping(source = "feelWheel.id", target = "feelWheelId")
    FeelingDTO toDto(Feeling feeling);

    @Mapping(source = "feelWheelId", target = "feelWheel")
    Feeling toEntity(FeelingDTO feelingDTO);

    default Feeling fromId(Long id) {
        if (id == null) {
            return null;
        }
        Feeling feeling = new Feeling();
        feeling.setId(id);
        return feeling;
    }
}
