package org.srs.apps.service.mapper;

import org.srs.apps.domain.*;
import org.srs.apps.service.dto.FeelWheelDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity FeelWheel and its DTO FeelWheelDTO.
 */
@Mapper(componentModel = "spring", uses = {EmployeeMapper.class})
public interface FeelWheelMapper extends EntityMapper<FeelWheelDTO, FeelWheel> {

    @Mapping(source = "employee.id", target = "employeeId")
    FeelWheelDTO toDto(FeelWheel feelWheel);

    @Mapping(target = "feelings", ignore = true)
    @Mapping(source = "employeeId", target = "employee")
    FeelWheel toEntity(FeelWheelDTO feelWheelDTO);

    default FeelWheel fromId(Long id) {
        if (id == null) {
            return null;
        }
        FeelWheel feelWheel = new FeelWheel();
        feelWheel.setId(id);
        return feelWheel;
    }
}
