package org.srs.apps.service.dto;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A DTO for the Employee entity.
 */
@Data
@EqualsAndHashCode()
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDTO implements Serializable {

    private static final long serialVersionUID = -7265683817337840282L;
    @EqualsAndHashCode.Include
    private Long id;

    @NotNull
    private String email;

    private Long managerId;

    protected boolean canEqual(Object other){
        return getClass() == other.getClass();
    }

}
