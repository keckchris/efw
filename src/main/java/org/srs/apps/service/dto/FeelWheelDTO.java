package org.srs.apps.service.dto;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;

/**
 * A DTO for the FeelWheel entity.
 */
@Data
@EqualsAndHashCode()
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class FeelWheelDTO implements Serializable {

    private static final long serialVersionUID = 7858821300152388906L;
    @EqualsAndHashCode.Include
    private Long id;

    @NotNull
    private String subject;

    private Instant from;

    private Instant to;

    private Long employeeId;

    protected boolean canEqual(Object other){
        return getClass() == other.getClass();
    }

}
