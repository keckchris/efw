package org.srs.apps.service.dto;

import lombok.*;
import org.srs.apps.domain.enumeration.FeelType;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A DTO for the Feeling entity.
 */
@Data
@EqualsAndHashCode()
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class FeelingDTO implements Serializable {

    private static final long serialVersionUID = -8881378161907591274L;
    @EqualsAndHashCode.Include
    private Long id;

    @NotNull
    private FeelType feeltype;

    private Integer capacity;

    private Boolean isSpeechable;

    private Long feelWheelId;
    protected boolean canEqual(Object other){
        return getClass() == other.getClass();
    }


}
