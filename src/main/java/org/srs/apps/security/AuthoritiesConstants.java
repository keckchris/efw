package org.srs.apps.security;

import lombok.experimental.UtilityClass;

/**
 * Constants for Spring Security authorities.
 */
@UtilityClass
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";


}
