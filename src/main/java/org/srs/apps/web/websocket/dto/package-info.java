/**
 * Data Access Objects used by WebSocket services.
 */
package org.srs.apps.web.websocket.dto;
