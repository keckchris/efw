/**
 * View Models used by Spring MVC REST controllers.
 */
package org.srs.apps.web.rest.vm;
