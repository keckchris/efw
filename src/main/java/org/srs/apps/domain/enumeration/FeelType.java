package org.srs.apps.domain.enumeration;

/**
 * The FeelType enumeration.
 */
public enum FeelType {
    ANGRY, SAD, HAPPY
}
