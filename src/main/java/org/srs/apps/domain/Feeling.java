package org.srs.apps.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;
import org.srs.apps.domain.enumeration.FeelType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A Feeling.
 */
@Entity
@Table(name = "feeling")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "feeling")
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class Feeling implements Serializable {


    private static final long serialVersionUID = 8373527571672143106L;
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "feeltype", nullable = false)
    private FeelType feeltype;

    @Column(name = "capacity")
    private Integer capacity;

    @Column(name = "is_speechable")
    private Boolean isSpeechable;

    @ManyToOne
    @JsonIgnoreProperties("feelings")
    private FeelWheel feelWheel;

    protected boolean canEqual(Object other){
        return getClass() == other.getClass();
    }

}
