import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { EfwFeelingModule } from './feeling/feeling.module';
import { EfwFeelWheelModule } from './feel-wheel/feel-wheel.module';
import { EfwEmployeeModule } from './employee/employee.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        EfwFeelingModule,
        EfwFeelWheelModule,
        EfwEmployeeModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EfwEntityModule {}
