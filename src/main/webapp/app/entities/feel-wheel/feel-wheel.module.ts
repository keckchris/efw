import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EfwSharedModule } from 'app/shared';
import {
    FeelWheelComponent,
    FeelWheelDetailComponent,
    FeelWheelUpdateComponent,
    FeelWheelDeletePopupComponent,
    FeelWheelDeleteDialogComponent,
    feelWheelRoute,
    feelWheelPopupRoute
} from './';

const ENTITY_STATES = [...feelWheelRoute, ...feelWheelPopupRoute];

@NgModule({
    imports: [EfwSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        FeelWheelComponent,
        FeelWheelDetailComponent,
        FeelWheelUpdateComponent,
        FeelWheelDeleteDialogComponent,
        FeelWheelDeletePopupComponent
    ],
    entryComponents: [FeelWheelComponent, FeelWheelUpdateComponent, FeelWheelDeleteDialogComponent, FeelWheelDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EfwFeelWheelModule {}
