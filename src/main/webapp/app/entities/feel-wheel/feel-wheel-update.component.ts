import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';

import { IFeelWheel } from 'app/shared/model/feel-wheel.model';
import { FeelWheelService } from './feel-wheel.service';
import { IEmployee } from 'app/shared/model/employee.model';
import { EmployeeService } from 'app/entities/employee';

@Component({
    selector: 'jhi-feel-wheel-update',
    templateUrl: './feel-wheel-update.component.html'
})
export class FeelWheelUpdateComponent implements OnInit {
    feelWheel: IFeelWheel;
    isSaving: boolean;

    employees: IEmployee[];
    from: string;
    to: string;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected feelWheelService: FeelWheelService,
        protected employeeService: EmployeeService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ feelWheel }) => {
            this.feelWheel = feelWheel;
            this.from = this.feelWheel.from != null ? this.feelWheel.from.format(DATE_TIME_FORMAT) : null;
            this.to = this.feelWheel.to != null ? this.feelWheel.to.format(DATE_TIME_FORMAT) : null;
        });
        this.employeeService.query().subscribe(
            (res: HttpResponse<IEmployee[]>) => {
                this.employees = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.feelWheel.from = this.from != null ? moment(this.from, DATE_TIME_FORMAT) : null;
        this.feelWheel.to = this.to != null ? moment(this.to, DATE_TIME_FORMAT) : null;
        if (this.feelWheel.id !== undefined) {
            this.subscribeToSaveResponse(this.feelWheelService.update(this.feelWheel));
        } else {
            this.subscribeToSaveResponse(this.feelWheelService.create(this.feelWheel));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IFeelWheel>>) {
        result.subscribe((res: HttpResponse<IFeelWheel>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackEmployeeById(index: number, item: IEmployee) {
        return item.id;
    }
}
