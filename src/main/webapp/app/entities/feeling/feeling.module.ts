import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EfwSharedModule } from 'app/shared';
import {
    FeelingComponent,
    FeelingDetailComponent,
    FeelingUpdateComponent,
    FeelingDeletePopupComponent,
    FeelingDeleteDialogComponent,
    feelingRoute,
    feelingPopupRoute
} from './';

const ENTITY_STATES = [...feelingRoute, ...feelingPopupRoute];

@NgModule({
    imports: [EfwSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        FeelingComponent,
        FeelingDetailComponent,
        FeelingUpdateComponent,
        FeelingDeleteDialogComponent,
        FeelingDeletePopupComponent
    ],
    entryComponents: [FeelingComponent, FeelingUpdateComponent, FeelingDeleteDialogComponent, FeelingDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EfwFeelingModule {}
