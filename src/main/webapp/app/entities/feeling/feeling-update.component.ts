import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IFeeling } from 'app/shared/model/feeling.model';
import { FeelingService } from './feeling.service';
import { IFeelWheel } from 'app/shared/model/feel-wheel.model';
import { FeelWheelService } from 'app/entities/feel-wheel';

@Component({
    selector: 'jhi-feeling-update',
    templateUrl: './feeling-update.component.html'
})
export class FeelingUpdateComponent implements OnInit {
    feeling: IFeeling;
    isSaving: boolean;

    feelwheels: IFeelWheel[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected feelingService: FeelingService,
        protected feelWheelService: FeelWheelService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ feeling }) => {
            this.feeling = feeling;
        });
        this.feelWheelService.query().subscribe(
            (res: HttpResponse<IFeelWheel[]>) => {
                this.feelwheels = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.feeling.id !== undefined) {
            this.subscribeToSaveResponse(this.feelingService.update(this.feeling));
        } else {
            this.subscribeToSaveResponse(this.feelingService.create(this.feeling));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IFeeling>>) {
        result.subscribe((res: HttpResponse<IFeeling>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackFeelWheelById(index: number, item: IFeelWheel) {
        return item.id;
    }
}
