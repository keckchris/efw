import { Moment } from 'moment';
import { IFeeling } from 'app/shared/model//feeling.model';

export interface IFeelWheel {
    id?: number;
    subject?: string;
    from?: Moment;
    to?: Moment;
    feelings?: IFeeling[];
    employeeId?: number;
}

export class FeelWheel implements IFeelWheel {
    constructor(
        public id?: number,
        public subject?: string,
        public from?: Moment,
        public to?: Moment,
        public feelings?: IFeeling[],
        public employeeId?: number
    ) {}
}
