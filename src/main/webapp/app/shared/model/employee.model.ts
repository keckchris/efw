import { IFeelWheel } from 'app/shared/model//feel-wheel.model';

export interface IEmployee {
    id?: number;
    email?: string;
    feelWheels?: IFeelWheel[];
    managerId?: number;
}

export class Employee implements IEmployee {
    constructor(public id?: number, public email?: string, public feelWheels?: IFeelWheel[], public managerId?: number) {}
}
